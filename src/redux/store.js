import { createStore } from 'redux';
import Reducer from './reducers';

const initialState = {
    products: [],
    car: {},
    items:[]
    // shoppingCart: []
};

const store = createStore( Reducer, initialState );

export default store;


// {sku:"1",name:"Zapato", car: 0,description:"Zapatos comodos", price: 300, stock: 10, image:"https://constantmotions.files.wordpress.com/2015/04/interstellar-fan-poster-nolan.jpg"},
//cantidad ,articulo, precio. articulo se pone atraves de un id
