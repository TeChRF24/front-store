import React, { useState, useEffect, useContext } from "react";
import createAuth0Client from "@auth0/auth0-spa-js";

export const Auth0Context = React.createContext();
export const useAuth0 = () => useContext(Auth0Context);
export const Auth0Provider = (props) => {
    const [auth0Client, setAuth0] = useState();
    const [isAuthenticated, setIsAuthenticated] = useState();
    const [user, setUser] = useState();
    const [loading, setLoading] = useState(true);
    const onRedirectCallback = appState => {
      window.history.replaceState(
        {},
        document.title,
        appState && appState.targetUrl
          ? appState.targetUrl
          : window.location.pathname
      );
    };

        useEffect(()=>{
        (async function() {
            const auth0FromHook = await createAuth0Client({
            domain: 'techrf24.auth0.com',
            client_id: '4HQklIY0k9umMsHorClXoBn5PQWJHbOI',
            redirect_uri:window.location.origin
            });
            setAuth0(auth0FromHook);

            if(window.location.search.includes("code=")){
              const { appState } = await auth0FromHook.handleRedirectCallback();
              onRedirectCallback(appState);
              console.log("Imprimiendo desde useEffect");
              console.log(appState);
            }

            const isAuthenticated = await auth0FromHook.isAuthenticated();
            setIsAuthenticated(isAuthenticated);
            
            if (isAuthenticated) {
              const user = await auth0FromHook.getUser();
              setUser(user);
            }

            setLoading(false);
        }());
               
        },[]);

        const handleRedirectCallback = async () => {
          setLoading(true);
          await auth0Client.handleRedirectCallback();
          const user = await auth0Client.getUser();
          setLoading(false);
          setIsAuthenticated(true);
          setUser(user);
          console.log("Imprimiendo desde el handleRedirectCallback");
        };
  return (
    <Auth0Context.Provider
      value={{
        cliente: auth0Client,
        isAuthenticated,
        handleRedirectCallback,
        user,
        loading
      }}
    >
      {props.children}
    </Auth0Context.Provider>
  );
};

// http://localhost:3000/?code=SXRrEqfs-6uAvlco&state=cEtURVVQcmR4QUw5LVY1T01VRmZMOGgzSFNKWUhVUXd%2BenBhcjhHUGZUSg%3D%3D

