import React from 'react';
import { BrowserRouter as Router, Route,  Switch } from "react-router-dom";
import Home from './pages/home';
import Products from './pages/products';
import PrivateRoute from './components/private-route';
import Payment from './pages/payment';
import Cancel from './pages/cancel';
import Success from './pages/success';
import { useAuth0 } from "./auth-wrapper";
function App() {
  const {loading} = useAuth0();
    if(loading){
      document.body.style.marginTop=0;
        return(
            <div style={{width:"100%", height:"100vh", display:"flex",justifyContent:"center",alignItems:"center",backgroundColor:"black",color:"white"}}>
                <p>Loading</p>
            </div>
        )
    }else{
      return (
          <Router>
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/products" component={Products}/>
              <PrivateRoute path="/success" component={Success}/>
              <PrivateRoute path="/cancel" component={Cancel}/>
              <PrivateRoute path="/payment" component={Payment}/>
            </Switch>
          </Router>
      );
    }
}
export default App;
