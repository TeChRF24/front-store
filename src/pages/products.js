import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
// import {Link} from 'react-router-dom';
import Nav from "../components/nav";
import {gql} from 'apollo-boost';
import axios from 'axios';
import { useAuth0 } from "../auth-wrapper";
import './css/products.css';
import {client} from '../index';
import Item from './items';
function mapStateToProps(state){
    return{
        products: state.products,
        car: state.car
    }
}
function Products({products, ...rest}){
  const {isAuthenticated,cliente} = useAuth0();
  // console.log(rest);
    // const defaultImg = "https://www.thenational.ae/image/policy:1.796084:1557836984/07189200.jpg?f=16x9&w=1200&$p$f$w=a9830af";
    // useEffect(()=>{
    //     client.query({
    //         query: gql`
    //           {
    //             Products{
    //               sku
    //               name
    //               description
    //               stock
    //               price
    //               image
    //             }
    //           }
    //         `
    //       }).then(response => {
    //           rest.dispatch({
    //             type:"PRODUCTS",
    //             payload: {
    //                 products: [...response.data.Products]
    //             }
    //           });
    //         // console.log(response.data.Products);
    //         // console.log(response.data.Products.length);
    //       });
    // },[rest.products]);
    if(window.location.pathname==="/products"){
        document.body.style.marginTop="10vh";
      }
    return(
        <>
        <Nav/>
        <div className="products-container" style={{display:"flex", flexWrap: "wrap", width:"100%", justifyContent:"space-around"}}>
            {
      products.map((e, index)=>{
        (async function Del(){
              const user = await cliente.getUser();
              let resp = await axios.post('http://localhost:8000/getcart',{user:user.email});
              let cart = resp.data;
              let id = e.sku.toString();
              if(cart.hasOwnProperty(id)){
                if(!e.stock){
                  delete cart[id];
                  let response = await axios.post('http://localhost:8000/cart', {
                    cart: cart,
                    user: user.email
                  });
                  rest.dispatch({
                    type: "CAR",
                    payload: {
                        car: cart
                    }
                   });
                }
            }
        }());
          if(e.stock){
               return(
            <div key={index} className="products-cards">
                <div className="products-image-container">
                    <p>{e.name}</p>
                    <img src={e.image} alt="" style={{height:"80%",width:"80%"}}/>
                </div>
                <div style={{display:"flex",alignItems:"center", flexDirection:"column", height:"20%",width:"100%"}}>
                    <div style={{display:"flex",width:"100%",height:"20%", justifyContent:"center",alignItems:'flex-end'}}>
                     <p>{e.description}</p>
                    </div>
                    <div style={{display:"flex",flexDirection:"row", backgroundColor:"red", width:"100%", height:"80%"}}>
                    <div style={{display:"flex",justifyContent:"space-around",alignItems:"center",backgroundColor:"brown",flex:"1", flexDirection:"column"}}>
                        <p>$ {e.price}</p>
                    </div>
                    <div style={{display:"flex",justifyContent:"center",alignItems:"center",backgroundColor:"orange",flex:"3"}}>
                       <div style={{display:'flex',flexDirection:'column'}}>
                            {/* <p style={{fontSize:'45%'}}>Disponibilidad: {e.stock}</p> */}
                           { isAuthenticated ? <Item product={e}/> : null }
                       </div>
                    </div> 
                    </div>
                </div>
            </div>
        );
        }else{
              return (null);
        }
    })
            }
        </div>
        </>
    );
}

export default connect(mapStateToProps)(Products);

// if(e.cantidad){
//     rest.dispatch({
//         type: "ADD_CAR",
//         index: index,
//         payload: {
//             cantidad: e.cantidad - 1,
//             car: e.car + 1
//         }
//     });
// }
// console.log(e.car);
// console.log(e.cantidad);