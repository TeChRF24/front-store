import React, {useEffect, useState} from 'react';
import axios from 'axios';
import { useAuth0 } from "../auth-wrapper";
import {connect} from 'react-redux';
function mapStateToProps(state){
    return{
        car: state.car
    }
}
function Item(props){
    console.log(props);
    const {cliente} = useAuth0();
      let [quantity,setQuantity] = useState(1);
      let [bask,setBask] = useState(0);
      useEffect(() => {
        (async function algo() {
          const user = await cliente.getUser();
          console.log(user.email);
          let respuesta = await axios.post('http://localhost:8000/getcart',{user:user.email});
          let carrito = respuesta.data;
          let id = props.product.sku.toString(); //console.log(id); console.log(cart);
          carrito[id] = (carrito[id] ? carrito[id]: 0);
          if(props.product.stock<carrito[id]){//5<8
            carrito[id]=props.product.stock;
            setBask(carrito[id]);
            props.dispatch({
              type: "CAR",
              payload: {
                  car: carrito
              }
             });
             let response = await axios.post('http://localhost:8000/cart', {
              cart: carrito,
              user: user.email
            });
          }else{
            setBask(carrito[id]);
          }
      }());
      },[]);
  
      function handleInputChange(event){ 
      setQuantity(event.target.value);
      }
      async function addToCart(){
          const user = await cliente.getUser();
          console.log(user.email);
          let resp = await axios.post('http://localhost:8000/getcart',{user:user.email});
          let cart = resp.data;
          let id = props.product.sku.toString(); //console.log(id); console.log(cart);
          cart[id] = (cart[id] ? cart[id]: 0);
          let qty = cart[id] + parseInt(quantity);
          if (props.product.stock < qty) { //5<0
            cart[id] = props.product.stock;
            setBask(cart[id]);
          } else {
            if(qty<1){
              cart[id]=0;
              setBask(cart[id]);
              delete cart[id];
            }else{
              cart[id] = qty;
              setBask(cart[id]);
            }
          }
          props.dispatch({
                type: "CAR",
                payload: {
                    car: cart
                }
               });
               let response = await axios.post('http://localhost:8000/cart', {
                cart: cart,
                user: user.email
              });
          console.log(response.data);
        }
        function myFun(evt){
              // var theEvent = evt || window.event;
              // console.log('here');
              // // Handle paste
              // if (theEvent.type === 'paste') {
              //   key = window.event.clipboardData.getData('text/plain');
              // } else {
              // // Handle key press
              //   var key = theEvent.keyCode || theEvent.which;
              //   key = String.fromCharCode(key);
              // }
              // var regex = /[0-9]|\./;
              // if( !regex.test(key) ) {
              // theEvent.returnValue = false;
              // if(theEvent.preventDefault) theEvent.preventDefault();
              // }
      }
      return(
          <>
          <div>
            <p style={{fontSize:'80%'}}>Disponibilidad: {props.product.stock}</p>
          </div>
          <div>
              <p>Agregados: {bask}</p>
              <button onClick={addToCart} style={{width:'50%'}}>Add</button>
              <input type="text" value={quantity} name="quantity" max={props.product.stock}
              onChange={handleInputChange} min="1" onKeyPress={myFun}
              style={{ width: "25%", marginRight: "10px", borderRadius: "3px"}} 
              />
          </div>
          </>
      )// display:'none' o visibility:'hidden'. readonly="readonly"
  }
  export default connect(mapStateToProps)(Item);
