import React from 'react';
import {useEffect,useState} from 'react';
// import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import axios from 'axios';
import Nav from '../components/nav';
import { useAuth0 } from '../auth-wrapper';
// import PaypalExpressBtn from 'react-paypal-express-checkout';
// import {credencials} from '../credencials';
import {gql} from 'apollo-boost';
import {client} from '../index';
function mapStateToProps(state){
    return {
        products: state.products,
        car: state.car
    }
}
function Payment(props){
    let [products,setProducts] = useState([]);
    let [total,setTotal] = useState(0);
    let [email,setEmail] = useState('');
    let [address,setAddress] = useState(false);
    let [loading,setLoading] = useState(true);
    const {cliente} = useAuth0();
    useEffect(()=>{
        (async function algo() {
            const user = await cliente.getUser();
            console.log(user.email);
            setEmail(user.email);
            axios.post('http://localhost:8000/getaddress',{
                correo: user.email
            }).then(e=>{
                setAddress(e.data);
                console.log(e.data);
            }).catch(err=>{
                console.log(err);
            });
        }());

        let items, figures=[], amount;
        client.query({
            query: gql`
              {
                Products{
                  sku
                  name
                  description
                  stock
                  price
                }
              }
            `
          }).then(response => {
              items = response.data.Products;
              console.log(response.data.Products);
            // console.log(response.data.Products.length);
            items = items.filter((e) => {
                let id;
                id = e.sku.toString();
                if(props.car.hasOwnProperty(id)){
                    e.stock = props.car[id];
                    return e;
                }
            });
            setProducts(items);
            figures = items.map(e=>{
                return e.stock*e.price;
            })
            amount = figures.reduce((total,num)=>{
                return total + num
            },0)
            setTotal(amount);
          });
          function loading(){
              setLoading(false);
          }
          setTimeout(loading,1100);
    },[props.car]);

    if(window.location.pathname==="/payment"){
        document.body.style.marginTop="10vh";
      }
    function PaymentButtons(){
        let cantidad = total;
        let currency = 'MXN';
        const env = 'sandbox';
        const client = {
            sandbox: 'AYKPNvSgprJShM6xY22-RS339pjQNefayKNjI8mfdsFyQ5mEtJ3k_e4x7igGAJ-NPVbUPzCFpOmcTuAb'
        };
        if(Object.keys(props.car).length>0){
            return(
            <div style={{display:"flex",justifyContent:"center", width:"70%"}}>
                {/* <PaypalExpressBtn env={env} client={client} currency={currency} total={cantidad} onError={onError} onSuccess={onSuccess} onCancel={onCancel} /> */}
                <form id="myform" style={{display:"none"}}>
                    <input type="number" value={total} name="total" readOnly/>
                    <input type="text" value={email} name="email" readOnly/>
                </form>
                <button style={{cursor:"pointer"}} type="submit" form="myform" formMethod="post" formAction="http://localhost:8000/paypal">
                    PAGAR con Paypal
                </button>
                <button style={{cursor:"pointer"}} type="submit" form="myform" formMethod="post" formAction="http://localhost:8000/openpay">
                    PAGAR con Openpay
                </button>
            </div>
            )
        }else{
            return null
        }
    }
    function Cart(){
        return(
            <div>
                <div>
                    <h2>Datos de Envío</h2>
                    <p>Calle Principal: {address.mainStreet}</p>
                    <p>Numero: {address.number}</p>
                    <p>Entre calle: {address.secondStreet}</p>
                    <p>Y calle: {address.thirdStreet}</p>
                    <p>Nombre de quien recibe: {address.receiver}</p>
                    <p>Estado: {address.state}</p>
                    <p>Ciudad: {address.city}</p>
                    <p>Código Postal: {address.zip}</p>
                    <div style={{display:'flex',justifyContent:'flex-end'}}>
                        <button style={{cursor:'pointer'}} onClick={()=>{
                            setAddress(false);
                        }}>Cambiar Dirección</button>
                    </div>
                </div>
            <table className="tableInfo">
                <caption className="">Lista de Compras</caption>
                <tbody>
                <tr>
                    <th>Cantidad</th>
                    <th>Nombre</th>
                    <th>Precio</th>
                    <th>Subtotal</th>
                </tr>
                {
                    products.map((e,i)=>{
                        return (
                            <tr key={i}>
                                <td>{e.stock}</td>
                                <td>{e.name}</td>
                                <td>${e.price}</td>
                                <td>${(e.stock)*(e.price)}</td>
                            </tr>
                        )
                    })
                }
                    <tr>
                        <td>TOTAL</td>
                        <td></td>
                        <td></td>
                        <td>${total}</td>
                    </tr>
                </tbody>
            </table>
            <PaymentButtons/>
            </div>
        )
    }
    function Streets(){
        let [mainStreet,setMainStreet] = useState('');//Datos de envio
        let [number,setNumber] = useState('');
        let [secondStreet,setSecondStreet] = useState('');
        let [thirdStreet,setThirdStreet] = useState('');
        let [receiver,setReceiver] = useState('');
        let [zip,setZip] = useState('');
        let [state,setState] = useState('');
        let [city,setCity] = useState('');
        return(
            <div style={{display:'flex',flexDirection:'column'}}>
            <div style={{display:"flex",justifyContent:'center'}}>
                <h3>Datos de Envio</h3>
            </div>
            <form action="http://localhost:8000/address" method="post">
                <div style={{display:'flex',justifyContent:'center',flexDirection:'column'}}>
                    <input type="text" value={email} name='email' readOnly style={{display:'none'}}/>
                <p>Calle:</p>
                <input type="text" name="mainStreet" value={mainStreet} onChange={(e)=>setMainStreet(e.target.value)} required/>
                <p>Numero:</p>
                <input type="text" name="number" value={number} onChange={(e)=>setNumber(e.target.value)} required/>
                <p>Segunda Calle:</p>
                <input type="text" name="secondStreet" value={secondStreet} onChange={(e)=>setSecondStreet(e.target.value)} required/>
                <p>Tercera Calle:</p>
                <input type="text" name="thirdStreet" value={thirdStreet} onChange={(e)=>setThirdStreet(e.target.value)} required/>
                <p>Nombre de quien recibe:</p>
                <input type="text" name="receiver" value={receiver} onChange={(e)=>setReceiver(e.target.value)} required/>
                <p>Código Postal:</p>
                <input type="text" name="zip" value={zip} onChange={(e)=>setZip(e.target.value)} required/>
                <p>Estado:</p>
                <input type="text" name="state" value={state} onChange={(e)=>setState(e.target.value)} required/>
                <p>Ciudad:</p>
                <input type="text" name="city" value={city} onChange={(e)=>setCity(e.target.value)} required/>
                </div>
                <div style={{display:'flex',justifyContent:'center'}}>
                    <input type="submit" value='Agregar Dirección' style={{cursor:'pointer'}}/>
                </div>
                </form>
        </div>
        )
    }
    return(
        <>
        <Nav/>
        <h1>Payment Page</h1>
        <p>Welcome User</p>
        {
        loading?(
        <div style={{display:'flex',justifyContent:'center'}}>
            <p>Cargando...</p>
        </div>):
        (<div>
            {
                products.length ? (
                    <>
                    {
                        address?<Cart/>:<Streets/>
                    }
                    </>
                ) : (
                    <p>No has agregado nada al carrito</p>
                )
            }
        </div>)
        }
        </>
    );
}
export default connect(mapStateToProps)(Payment);
