import React, {useState, useEffect} from "react";
import { Link } from 'react-router-dom';
import { withRouter } from "react-router";
import { useAuth0 } from "../auth-wrapper";
import {connect} from 'react-redux';
import axios from 'axios';
import {client} from '../index';
import {gql} from 'apollo-boost';
import './css/nav.css';
import { async } from "q";
// import { async } from "q";
function mapStateToProps(state){
    return{
        products: state.products,
        car: state.car
    }
}
function Nav(props){
    const {cliente, isAuthenticated} = useAuth0();
    let [items,setItems] = useState(0);
    console.log(props);
    console.log(props.car);
    useEffect(()=>{
        async function pet(){
            const user = await cliente.getUser();
            if(user){
                console.log(user.email);
                let respuesta = await axios.post('http://localhost:8000/getcart',{user:user.email});
                let carrito = respuesta.data;
                props.dispatch({
                    type: 'CAR',
                    payload: {
                        car: carrito
                    }
                    });
            }
        }
        pet();
    },[]);
    useEffect(()=>{
        client.query({
            query: gql`
              {
                Products{
                  sku
                  name
                  description
                  stock
                  price
                  image
                }
              }
            `
          }).then(response => {
              console.log(response.data.Products);
              props.dispatch({
                type:"PRODUCTS",
                payload: {
                    products: [...response.data.Products]
                }
              });
              //Start
              (async function Del(){
                const user = await cliente.getUser();
                if(user){
                    let respuesta = await axios.post('http://localhost:8000/getcart',{user:user.email});
                    let carrito = respuesta.data;
                    let cart = Object.keys(carrito);
                    let ids = response.data.Products.map((e)=>{
                        return e.sku;
                    });
                    cart.map((e)=>{
                        let id = e.toString();
                        console.log(id);
                        console.log(ids.includes(id));
                        if(!ids.includes(id)){
                            delete carrito[id];
                            (async function car(){
                                let response = await axios.post('http://localhost:8000/cart', {
                                    cart: carrito,
                                    user: user.email
                                });
                            }());
                        }
                    });
                    props.dispatch({
                        type: 'CAR',
                        payload: {
                            car: carrito
                        }
                    });
                }
              }());
          });
    },[]);
    useEffect(() => {
        (async function algo() {
            if(Object.values(props.car).length===0){
                setItems(0);
            }else{
                setItems(Object.values(props.car).reduce((total,num)=> total + num));
            }
        }());
      },[props.car]);
    // console.log(props.location.pathname);
    function PaymentButton(){
        if(items&&props.location.pathname!=="/payment"){
            return (<button onClick={()=>{
                console.log("Working!!");
                props.history.push("/payment");
            }} className="payment-button">PAGAR</button>
            )
        }else{
            return null
        }
    }
    // console.log(props);
    if(isAuthenticated){
        return(
            <nav  style={{display:"flex",width:"100%",backgroundColor:"orange",height:"10vh",textDecoration:"none",flexDirection:"row",position:"fixed",top:0}}>
                <div style={{width:"50%", backgroundColor:"orange",height:"100%", display:"flex",justifyContent:"flex-start"}}>
                <ul style={{display:"flex",justifyContent:"space-around",listStyle:"none",alignItems:"center", width:"25%",}}>
                    <li style={{fontSize:22}}>
                        <button onClick={()=>{
                            props.history.push("/");
                        }}>
                            Home
                        </button>
                    </li>
                    <li style={{fontSize:22}}>
                        <button onClick={()=>{
                        props.history.push("/products");
                        }}>
                            Products
                        </button>
                    </li>
                </ul>
                </div>
                <div style={{display:"flex",width:"50%",justifyContent:"flex-end",alignItems:"center"}}>
                            <PaymentButton/>
                            <p style={{fontSize:"1.2em", position:"relative",right:"7%",fontWeight:"bold"}}>N. Arts: {items}</p>
                            <button className="logout" onClick={async ()=>{
                                 await cliente.logout();
                            }}>Salir</button>
                </div>
            </nav>
        );
    }else{
        return(
            <nav  style={{display:"flex",width:"100%",backgroundColor:"orange",height:"10vh",textDecoration:"none",flexDirection:"row",position:"fixed",top:0}}>
                <div style={{width:"50%", backgroundColor:"orange",height:"100%", display:"flex",justifyContent:"flex-start"}}>
                <ul style={{display:"flex",justifyContent:"space-around",listStyle:"none",alignItems:"center", width:"25%",}}>
                    <li style={{fontSize:22}}><Link to="/">Home</Link></li>
                    <li style={{fontSize:22}}><Link to="/products">Productos</Link></li>
                </ul>
                </div>
                <div style={{display:"flex",width:"50%",justifyContent:"flex-end",alignItems:"center"}}>
                            <button className="login" onClick={async ()=>{
                                 await cliente.loginWithRedirect();
                            }}>Registrate o Ingresa</button>
                            {/* <button className="login" onClick={async ()=>{
                                 await cliente.loginWithRedirect();
                            }}>Registrate o Ingresa</button> */}
                </div>
            </nav>
        );
    }
}
export default withRouter(connect(mapStateToProps)(Nav));