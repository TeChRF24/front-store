import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import { useAuth0 } from "../auth-wrapper";

function PrivateRoute({component: Component, ...rest}){
    const {isAuthenticated} = useAuth0();
    if(isAuthenticated){
        return(
            <Route {...rest} render={rProps=>(
                    <Component {...rProps}/>
            )}/>
        )
    }else{
        return(
            <Redirect to="/"/>
        )
    }
}

export default PrivateRoute;